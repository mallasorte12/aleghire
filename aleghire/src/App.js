import Navbar from "./components/nav/Navbar";
import {Switch, BrowserRouter as Router, Route} from 'react-router-dom'
import Home from "./components/pages/Home";
import City from "./components/pages/City";
import Work from './components/pages/Work'
import './style.css'

function App() {
  return (
    <div className="App">
        <Router>
            <Navbar/>
            <Switch>
                <Route path='/home' exact component={Home}/>
                <Route path='/work' exact component={Work}/>
                <Route path='/city' exact component={City}/>
            </Switch>
        </Router>

    </div>
  );
}

export default App;
