import React from 'react'

export default function PlayerInfo(){

    return (
        <div className='player-info'>
            <img src="https://i.pinimg.com/564x/c9/0f/1f/c90f1ff53d0a69a802fa2cb804be1684.jpg" className='pfp'/>
            <div className='player-info-text'>
                Level: 4556 <br/>
                $300,242,567
            </div>
        </div>
    )

}