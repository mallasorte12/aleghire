import React from 'react'
import './navbar.css'
import {Link} from 'react-router-dom'
import PlayerInfo from "../PlayerInfo";


export default function Navbar () {
    return (
        <nav className='navbar'>
            <PlayerInfo/>
            <ul className='navbar-list'>

                <li className='nav-item'><Link to='/home' className='nav-link'>Home</Link></li>
                <li className='nav-item'><Link to='/work' className='nav-link'>Work</Link></li>
                <li className='nav-item'><Link to='/city' className='nav-link'>City</Link></li>
            </ul>
        </nav>
    )
}